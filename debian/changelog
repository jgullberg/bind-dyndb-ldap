bind-dyndb-ldap (11.10-5) unstable; urgency=medium

  * control, patches: Fix build against bind9 9.18.13.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 20 Mar 2023 11:27:22 +0200

bind-dyndb-ldap (11.10-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 12 to 13.
    + debian/rules: Drop --fail-missing argument to dh_missing, which is now the
      default.
  * Update standards version to 4.6.2, no changes needed.

  [ Jakob Haufe ]
  * Switch to bind 9.18.12

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 21 Feb 2023 10:36:39 +0200

bind-dyndb-ldap (11.10-3) unstable; urgency=medium

  [ Jakob Haufe ]
  * Import patch from https://pagure.io/bind-dyndb-ldap/pull-request/218
  * Switch to bind 9.18.11

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 02 Feb 2023 22:51:41 +0200

bind-dyndb-ldap (11.10-2) unstable; urgency=medium

  * patches: Fix build against bind9 9.18.10, and bump the hardcoded
    version to match. (Closes: #1027094)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 12 Jan 2023 15:08:57 +0200

bind-dyndb-ldap (11.10-1) unstable; urgency=medium

  * New upstream release.
  * patches: Drop upstreamed patches.
  * rules: Don't force the libdir to /usr/lib. Named can find plugins
    from the plugin dir without specifying the full path.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 22 Jun 2022 17:37:46 +0300

bind-dyndb-ldap (11.9-5) unstable; urgency=medium

  * support-9.18.diff: Fix build with bind9 9.18. (Closes: #1006014)
    - drop patches that aren't needed anymore with this
  * control, rules: Use a strict dependency on bind9-libs that the
    package was built against, in order to avoid bind9 updates breaking
    the package. (Closes: #1004729)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 23 Feb 2022 13:17:07 +0200

bind-dyndb-ldap (11.9-4) unstable; urgency=medium

  * Rebuild against bind9 9.17.19-3. (Closes: #998575, #999463)
  * Fix keytab name.
  * hardcode-version.diff: Hardcode the abi version.
  * fix-atomics.diff: Set GNUC_ATOMICS.
  * use-isc_result_totext.diff: Migrate to isc_result_totext.
  * use-dns_name_copy.diff: Use dns_name_copy.
  * fix-typo.diff: Fix a typo.
  * rules: Add -Wno-uninitialized to CFLAGS.
  * use-dns_ssuruletype_t.diff: Migrate to dns_ssuruletype_t.
  * define-have-threads-h.diff: Adjust for current bind9.
  * drop-serialize.diff: dns_db_serialize is gone.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 17 Nov 2021 23:24:37 +0200

bind-dyndb-ldap (11.9-3) unstable; urgency=medium

  * Fix build with current bind9.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 20 Oct 2021 21:38:18 +0300

bind-dyndb-ldap (11.9-2) unstable; urgency=medium

  * Rebuild against current bind9.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 20 Oct 2021 21:28:52 +0300

bind-dyndb-ldap (11.9-1) unstable; urgency=medium

  * New upstream release.
  * Drop upstreamed patches.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 16 Aug 2021 09:37:16 +0300

bind-dyndb-ldap (11.8-1) experimental; urgency=medium

  [ Sergio Durigan Junior ]
  * d/p/fix-ftbfs-openldap-2.5.diff: Fix FTBFS when building with
    OpenLDAP 2.5.

  [ Timo Aaltonen ]
  * New upstream release.
  * Drop upstreamed patches.
  * fix-ftbfs.diff: Fix the config include.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 14 May 2021 10:56:50 +0300

bind-dyndb-ldap (11.6-3) unstable; urgency=medium

  * support-9.16.13.diff: Fix build against bind 9.16.13 and up.
    (Closes: #986509)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 04 May 2021 19:34:58 +0300

bind-dyndb-ldap (11.6-2) unstable; urgency=medium

  * support-bind-9.16.11.diff: Fix build against bind9 9.16.11 and .11.
    (Closes: #981100)

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 15 Feb 2021 12:47:06 +0200

bind-dyndb-ldap (11.6-1) unstable; urgency=medium

  * New upstream release.
  * support-9.16.9.diff: Support bind 9.16.9.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 27 Nov 2020 09:59:37 +0200

bind-dyndb-ldap (11.5-2) unstable; urgency=medium

  * Rebuild against current bind9. (Closes: #974741)

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 15 Nov 2020 09:20:24 +0200

bind-dyndb-ldap (11.5-1) unstable; urgency=medium

  * New upstream release.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 25 Sep 2020 10:54:56 +0300

bind-dyndb-ldap (11.4-1) unstable; urgency=medium

  * New upstream release.
  * bind-9.16-support.diff: Dropped, upstream.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 18 Sep 2020 12:01:09 +0300

bind-dyndb-ldap (11.3-2) unstable; urgency=medium

  * bind-9.16-support.diff: Attempt to fix build on 32bit.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 17 Sep 2020 13:38:20 +0300

bind-dyndb-ldap (11.3-1) unstable; urgency=medium

  * New upstream release.
  * fix-werror-build.diff: Refreshed.
  * bind-9.16-support.diff: Support bind 9.16. (LP: #1874568)
  * control: Build-depend on bind9-dev. (Closes: #942499)
  * source: Extend diff-ignore.
  * control: Bump policy to 4.5.0.
  * control: Migrate to debhelper-compat, bump to 12.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 06 Aug 2020 15:20:35 +0300

bind-dyndb-ldap (11.2-1) unstable; urgency=medium

  * New upstream release.
  * add-empty-fallback.diff, add-int.h-include.diff, use-correct-dn-value.diff,
    0003-Support-for-BIND-9.11.3.patch: Dropped, upstream
  * source: Update extend-diff-ignore.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 05 Nov 2019 20:53:31 +0200

bind-dyndb-ldap (11.1-6) unstable; urgency=medium

  * use-correct-dn-value.diff: Fix ftbfs with gcc9. (Closes: #925639)

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 15 Aug 2019 19:39:12 +0300

bind-dyndb-ldap (11.1-5) unstable; urgency=medium

  [ Peter Michael Green ]
  * Add '#include <isc/int.h>' in src/types.h and src/zone.c.
    (Closes: 911976)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 23 Nov 2018 07:09:40 +0200

bind-dyndb-ldap (11.1-4) unstable; urgency=medium

  * postinst: Migrate old style named.conf to new.
  * control: Update the maintainer address.
  * Bump policy to 4.15, dh to 11.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 31 Jul 2018 23:44:13 +0300

bind-dyndb-ldap (11.1-3) unstable; urgency=medium

  * control: Update VCS urls.
  * rules: Don't use multiarch path.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 16 Apr 2018 13:02:48 +0300

bind-dyndb-ldap (11.1-2) unstable; urgency=medium

  * Fix build with bind 9.11.3. (Closes: #894091)

 -- Timo Aaltonen <tjaalton@debian.org>  Sun, 08 Apr 2018 21:59:56 +0300

bind-dyndb-ldap (11.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #879069)
  * control: Bump bind9 dependency.
  * patches: Drop obsolete patches, refresh others.
  * add-empty-fallback.diff: Fix build with current bind9.
  * source/options: Ignore files in contrib.

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 08 Dec 2017 13:33:52 +0200

bind-dyndb-ldap (10.1-2) unstable; urgency=medium

  * control, copyright, watch: Update upstream urls.
  * gcc7-ftbfs-fix.diff: Fix build with gcc7. (Closes: #853326)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 30 Aug 2017 00:26:59 +0300

bind-dyndb-ldap (10.1-1) unstable; urgency=medium

  * New upstream release.
  * control: Bump policy to 3.9.8, no changes.
  * control: Update vcs urls to use https, cgit.
  * Refresh patches, drop fix-gcc6-ftbfs.diff.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 19 Sep 2016 21:11:47 +0300

bind-dyndb-ldap (8.0-5) unstable; urgency=medium

  * Use -fno-delete-null-pointer-checks to fix build with gcc-6.
   (Closes: #822706)
  * fix-werror-build.diff: Actually make the build reproducible instead
    of just fixing the error. (Closes: #809565)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 27 Apr 2016 09:13:01 +0300

bind-dyndb-ldap (8.0-4) unstable; urgency=medium

  * Add missing /var/cache/bind/dynamic with correct permissions.

 -- Timo Aaltonen <tjaalton@debian.org>  Thu, 14 Apr 2016 15:43:35 +0300

bind-dyndb-ldap (8.0-3) unstable; urgency=medium

  * workaround-missing-headers.patch: libbind-dev actually ships
    dns/forward.h and dns/update.h now, so ship only errno2result.h
    here.

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 05 Apr 2016 07:59:43 +0300

bind-dyndb-ldap (8.0-2) experimental; urgency=medium

  [ Timo Aaltonen ]
  * rules: Revert to using multiarch install path so that bind can
    find the plugin without further configuration.
  * rules, install: Install via debian/tmp.
  * control: Bump libbind-dev build-dep and bind9 dep to 9.10.3 for the
    multiarch switch.

  [ Matthias Klose ]
  * fix-werror-build.diff: Fix build failure with -Werror.
    (Closes: #809565)
  * workaround-missing-headers.patch: Updated files from current bind9
    (Closes: #815167)

 -- Timo Aaltonen <tjaalton@debian.org>  Tue, 08 Mar 2016 14:26:34 +0200

bind-dyndb-ldap (8.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #790227, #806778)
  * control: Mark FreeIPA team as maintainer.
  * control: Add uuid-dev to build-depends.
  * workaround-missing-headers.patch: Update to not check for
    isc/errno2result.h during configure.
  * rules, postinst, dirs: Modify cache directory permissions on
    postinst.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 09 Dec 2015 10:25:18 +0200

bind-dyndb-ldap (6.0-4) unstable; urgency=medium

  * control: Depend on bind9. (Closes: #768392)

 -- Timo Aaltonen <tjaalton@debian.org>  Fri, 07 Nov 2014 08:15:27 +0200

bind-dyndb-ldap (6.0-3) unstable; urgency=medium

  * rules: Force libdir as /usr/lib so that named finds the plugin from
    $libdir/bind/ldap.so.
  * fix-keytab-path.diff: Use the correct path to named.keytab.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 05 Nov 2014 18:25:44 +0200

bind-dyndb-ldap (6.0-2) unstable; urgency=medium

  * rules, postinst: Create /var/cache/bind/dyndb-ldap owned by bind user.

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 05 Nov 2014 14:44:31 +0200

bind-dyndb-ldap (6.0-1) unstable; urgency=medium

  * New upstream release.

 -- Timo Aaltonen <tjaalton@debian.org>  Mon, 06 Oct 2014 11:48:04 +0300

bind-dyndb-ldap (5.1-1) unstable; urgency=low

  * Initial release (Closes: #734706)

 -- Timo Aaltonen <tjaalton@debian.org>  Wed, 01 Oct 2014 15:30:06 +0200
